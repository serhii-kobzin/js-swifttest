//
//  ViewController.swift
//  JS-SwiftTest
//
//  Created by Serhii Kobzin on 5/14/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    
    private var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let handleFinishEditingOfTextareaScriptCode = "var textarea = document.querySelector('textarea'); textarea.addEventListener(\"focusout\", function(event){ webkit.messageHandlers.finishEditingOfTextarea.postMessage(textarea.value); });"
        
        let handleClickOnFileUploadButtonScriptCode = "document.querySelector('input[accept=\"application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*, video/*\"]').addEventListener(\"click\", function(event){ webkit.messageHandlers.clickOnFileUploadButton.postMessage(\"\"); event.preventDefault(); });"
        
        let handleClickOnImageProofUploadButtonScriptCode = "document.querySelector('input[accept=\"image/*\"]').addEventListener(\"click\", function(event){ webkit.messageHandlers.clickOnOnImageProofUploadButton.postMessage(\"\"); event.preventDefault(); });"
        
        let handleClickOnVideoProofUploadButtonScriptCode = "document.querySelector('input[accept=\"video/*\"]').addEventListener(\"click\", function(event){ webkit.messageHandlers.clickOnOnVideoProofUploadButton.postMessage(\"\"); event.preventDefault(); });"
        
        let contentController = WKUserContentController()
        
        let handleFinishEditingOfTextareaScript = WKUserScript(source: handleFinishEditingOfTextareaScriptCode, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        contentController.addUserScript(handleFinishEditingOfTextareaScript)
        let handleClickOnFileUploadButtonScript = WKUserScript(source: handleClickOnFileUploadButtonScriptCode, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        contentController.addUserScript(handleClickOnFileUploadButtonScript)
        let handleClickOnImageProofUploadButtonScript = WKUserScript(source: handleClickOnImageProofUploadButtonScriptCode, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        contentController.addUserScript(handleClickOnImageProofUploadButtonScript)
        let handleClickOnVideoProofUploadButtonScript = WKUserScript(source: handleClickOnVideoProofUploadButtonScriptCode, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        contentController.addUserScript(handleClickOnVideoProofUploadButtonScript)
        
        contentController.add(self, name: "finishEditingOfTextarea")
        contentController.add(self, name: "clickOnFileUploadButton")
        contentController.add(self, name: "clickOnOnImageProofUploadButton")
        contentController.add(self, name: "clickOnOnVideoProofUploadButton")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        webView = WKWebView(frame: view.bounds, configuration: config)
        view.addSubview(webView)
        
        guard let url = URL(string: "https://demo.co-factor.com/missions.input.types.html") else {
            return
        }
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
}

extension ViewController: WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case "finishEditingOfTextarea":
            print("Inputed Text \(message.body)")
        case "clickOnFileUploadButton":
            print("Pressed File Upload Button")
        case "clickOnOnImageProofUploadButton":
            print("Pressed Image Proof Upload Button")
        case "clickOnOnVideoProofUploadButton":
            print("Pressed Video Proof Upload Button")
        default:
            break
        }
    }
    
}
